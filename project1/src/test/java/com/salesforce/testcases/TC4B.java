package com.salesforce.testcases;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.salesforce.utilities.BrowserUtilities;
import com.salesforce.utilities.DataUtilities;
import com.salesforce.utilities.FunctionalUtility;
import com.salesforce.utilities.ReportUtilities;

public class TC4B {
	public static FunctionalUtility fun = new FunctionalUtility();
	public static BrowserUtilities browser = new BrowserUtilities();
	public static DataUtilities datautil = new DataUtilities();
	public static ReportUtilities rep = new ReportUtilities();
	
	public static void tc4BLoginInvalidCredentials() throws IOException
	{
		String[][] data = datautil.readXlData("D:\\TechArch\\salesforcetestngdemo\\salesforcepro_excel\\TC4B_LoginInvalidCredentials.xls", "Sheet1");
		String UserName = data[1][0];
		String password = data[1][1];
		WebElement Uname = browser.driver.findElement(By.xpath("//input[@id='username']"));
		fun.enterText(Uname, UserName, "UserName");
		WebElement pwd = browser.driver.findElement(By.id("password"));
		fun.enterText(pwd, password, "Password");
		WebElement login = browser.driver.findElement(By.name("Login"));
		fun.clickBttn(login, "login");

		 
  //getting Error Message
  WebElement errMessage1 = browser.driver.findElement(By.id("error"));
  
  //Validating Error Message 
  String errMsg = "Please check your username and password. If you still can't log in, contact your Salesforce administrator.";
  System.out.println(errMsg);
  fun.errMessage(errMessage1,errMsg);
	}

}
