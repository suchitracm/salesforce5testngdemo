package com.salesforce.testcases;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.salesforce.utilities.BrowserUtilities;
import com.salesforce.utilities.DataUtilities;
import com.salesforce.utilities.FunctionalUtility;

//
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//
public class TC1 {
	public static FunctionalUtility fun = new FunctionalUtility();
	public static BrowserUtilities browser = new BrowserUtilities();
	public static DataUtilities datautil = new DataUtilities();
	
	public static void tc1loginerrormessage() throws IOException
	{
		String[][] data = datautil.readXlData("D:\\TechArch\\salesforcetestngdemo\\salesforcepro_excel\\TC2_LoginValidCredentials.xls", "Sheet1");
		String UserName = data[0][1];
		String password = data[1][1];
		WebElement Uname = browser.driver.findElement(By.xpath("//input[@id='username']"));
		fun.enterText(Uname, UserName, "UserName");
		WebElement pwd = browser.driver.findElement(By.id("password"));
		fun.enterText(pwd, password, "Password");
		pwd.clear();
		WebElement login = browser.driver.findElement(By.name("Login"));
		fun.clickBttn(login, "login");
		String errormessage = browser.driver.findElement(By.xpath("//div[@id='error']")).getText();
		System.out.println(errormessage);

		// getting Error Message
		WebElement errMessage = browser.driver.findElement(By.id("error"));

		// Validating Error Message
		String errMsg = "Please enter your password.";
		fun.errMessage(errMessage, errMsg);
	}
	
	

}
