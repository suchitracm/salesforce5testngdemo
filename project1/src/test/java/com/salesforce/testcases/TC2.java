package com.salesforce.testcases;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;
import com.salesforce.utilities.BrowserUtilities;
import com.salesforce.utilities.DataUtilities;
import com.salesforce.utilities.FunctionalUtility;
import com.salesforce.utilities.ReportUtilities;

public class TC2 {
	
	public static FunctionalUtility fun = new FunctionalUtility();
	public static BrowserUtilities browser = new BrowserUtilities();
	public static DataUtilities datautil = new DataUtilities();
	public static ReportUtilities rep = new ReportUtilities();
	public static void tc2loginvalidcredentials() throws IOException
	{
		String[][] data = datautil.readXlData("D:\\TechArch\\salesforcetestngdemo\\salesforcepro_excel\\TC2_LoginValidCredentials.xls", "Sheet1");
		String UserName = data[0][1];
		String password = data[1][1];
		WebElement Uname = browser.driver.findElement(By.xpath("//input[@id='username']"));
		fun.enterText(Uname, UserName, "UserName");
		WebElement pwd = browser.driver.findElement(By.id("password"));
		fun.enterText(pwd, password, "Password");
		WebElement login = browser.driver.findElement(By.name("Login"));
		fun.clickBttn(login, "login");

		// getting user profile name
		WebElement userProfileName1 = browser.driver.findElement(By.id("userNavLabel"));
		// checking whether the user profile name is same as the user name

		if (userProfileName1.getText().contains("suchi")) {
			System.out.println("The Profile User Name is " + userProfileName1.getText());
			System.out.println("The User Name is Verified");
			rep.Logger.log(LogStatus.PASS, "Username is verified successfully");
		} else {
			System.out.println("The User Name is not correct.Verified");
			rep.Logger.log(LogStatus.FAIL, "User Name is not matching" + rep.Logger.addScreenCapture(rep.takeScreenshot()));
		}
		fun.clickBttn(userProfileName1, "Profile User Name");
		// Logout from the application
		WebElement logoutMenu1 = browser.driver.findElement(By.xpath("//*[@id='userNav-menuItems']/a[5]"));
		fun.clickBttn(logoutMenu1, "Logout Button");

	}

}
