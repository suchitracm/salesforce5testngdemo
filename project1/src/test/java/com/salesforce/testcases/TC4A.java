package com.salesforce.testcases;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;
import com.salesforce.utilities.BrowserUtilities;
import com.salesforce.utilities.DataUtilities;
import com.salesforce.utilities.FunctionalUtility;
import com.salesforce.utilities.ReportUtilities;

public class TC4A {
	public static FunctionalUtility fun = new FunctionalUtility();
	public static BrowserUtilities browser = new BrowserUtilities();
	public static DataUtilities datautil = new DataUtilities();
	public static ReportUtilities rep = new ReportUtilities();
	
	public static void TC4A_ForgotPassword() throws IOException
	{
		  //Click Forgot Password
		  WebElement forgotPassword = browser.driver.findElement(By.id("forgot_password_link"));
		  fun.clickBttn(forgotPassword,"Forgot Password Link");
		  
		  
		  //getting Forgot Password Page header
		  WebElement forgotPassPage = browser.driver.findElement(By.id("header"));
		  
		  //checking whether the user is able to see the Forgot Your Password page 
		  if(forgotPassPage.getText().equals("Forgot Your Password")) 
		  {
		  System.out.println("The user is able to see "+ forgotPassPage.getText());
		  System.out.println("The Forgot Your Password Page Verified");
		  rep.Logger.log(LogStatus.PASS, "Forgot your Password page verified successfully");

		  }
		  else {
		  System.out.println("The Forgot Your Password Page is not Visible. Verified");
		rep.Logger.log(LogStatus.FAIL, "The Forgot your Password page is not visible" +rep.Logger.addScreenCapture(rep.takeScreenshot()));

		  }
		  
		  //click cancel button
		  WebElement cancelButton = browser.driver.findElement(By.xpath("//*[@id=\'forgotPassForm\']/div[1]/a"));
		  fun.clickBttn(cancelButton,"Cancel Button");
	}

}
