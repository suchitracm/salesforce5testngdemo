package com.salesforce.utilities;

import java.io.IOException;

import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import freemarker.log.Logger;

public class FunctionalUtility extends ReportUtilities {

	public static ReportUtilities reputility = new ReportUtilities();
	
	
	/*
	 * Method Name	: enterText
	 * Method Description	:	To enter text in a Web Element
	 * Arguments			:	ele 		- WebElement
	 * 							uText 		- Text to be entered
	 * 							fieldName 	- Name of the WebElement(Text Field)
	 * Date Created			:	12/7/2019
	 * Date Modified		:	12/7/2019
	 */
	public static void enterText(WebElement ele,String uText,String fieldName) throws IOException
	{
		if(ele.isDisplayed())
		{
			System.out.println(fieldName+" is displayed on the screen");
			System.out.println(uText+" is successfully entered into "+fieldName);
			ele.clear();
			ele.sendKeys(uText);
			reputility.Logger.log(LogStatus.INFO,fieldName+" entered successfully");

		}
		else
		{
			System.out.println(uText +" is not successfully entered into "+fieldName);
			reputility.Logger.log(LogStatus.FAIL,fieldName+" is not entered successfully"+reputility.Logger.addScreenCapture(reputility.takeScreenshot()));

		}
	}

	
	
	
	/*
	 * Method Name			:   clickBttn
	 * Method Description	:	To click button
	 * Arguments			:	ele 		- WebElement
	 * 							fieldName 	- Name of the Button
	 * Date Created			:	12/7/2019
	 * Date Modified		:	12/7/2019
	 */
	public static void clickBttn(WebElement ele,String fieldName) throws IOException
	{
		if(ele.isDisplayed())
		{
			System.out.println(fieldName+" is displayed on the screen");
			System.out.println(fieldName+" is successfully clicked ");
			ele.click();
			reputility.Logger.log(LogStatus.INFO,fieldName+" clicked successfully");

		}
		else
		{
			System.out.println(fieldName +" is not successfully clicked");
			reputility.Logger.log(LogStatus.FAIL,fieldName+" is not clicked successfully"+reputility.Logger.addScreenCapture(reputility.takeScreenshot()));

		}
	}
	
	/*
	 * Method Name			:  	errMessage
	 * Method Description	:	To validate error message
	 * Arguments			:	ele 		- WebElement
	 * 							erMessage 	- Error Message
	 * Date Created			:	12/7/2019
	 * Date Modified		:	12/7/2019
	 */
	public static void errMessage(WebElement ele1, String erMessage) throws IOException
	{
		if (ele1.getText().equalsIgnoreCase(erMessage))
		{
			System.out.println(erMessage+" error message popped");
			System.out.println("The error message is Verified");
			reputility.Logger.log(LogStatus.PASS,erMessage+" Error message displayed successfully");
		}
			else
			{
			System.out.println("The error message didn't pop.");
	       reputility.Logger.log(LogStatus.FAIL, " Error message is not displayed successfully."+reputility.Logger.addScreenCapture(reputility.takeScreenshot()));

			}
	}


	/*
	 * Method Name			:  	msgVerify
	 * Method Description	:	To validate message
	 * Arguments			:	ele 		- WebElement
	 * 							msgInfo 	- Message Info
	 * Date Created			:	12/7/2019
	 * Date Modified		:	12/7/2019
	 */
	public static void msgVerify(WebElement ele2, String msgInfo) throws IOException
	{
		if (ele2.getText().contains(msgInfo))
		{
			System.out.println(msgInfo+" is displayed");
			System.out.println("Successfully is Verified");
			reputility.Logger.log(LogStatus.PASS,msgInfo+" is verified successfully");

		}
			else
			{
			System.out.println("The info is not successfull.");
			reputility.Logger.log(LogStatus.FAIL,msgInfo+" is not matched"+reputility.Logger.addScreenCapture(reputility.takeScreenshot()));


			}
	}


	

	
	
	
}
