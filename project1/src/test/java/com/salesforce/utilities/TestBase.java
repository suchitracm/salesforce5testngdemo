
package com.salesforce.utilities;


import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import com.salesforce.testcases.TC1;
import com.salesforce.testcases.TC2;
import com.salesforce.testcases.TC3;
import com.salesforce.testcases.TC4A;
import com.salesforce.testcases.TC4B;
import com.salesforce.testcases.TC5;

public class TestBase {


	
	public static BrowserUtilities browser = new BrowserUtilities();
	public static DataUtilities datautil = new DataUtilities();
	public static FunctionalUtility fun = new FunctionalUtility();
	public static ReportUtilities rep = new ReportUtilities();
	public static TC1 tc1 = new TC1();
	public static TC3 tc3 = new TC3();
	public static TC4A tc4A = new TC4A();
	public static TC5 tc5 = new TC5();
	public static TC4B tc4B = new TC4B();
	static WebDriver driver;

	@Test(priority=1)
	public static void TC1_LoginErrorMessage() throws IOException, InterruptedException {
		System.out.println("TC1_LoginErrorMessage");
		
		
		//rep.Logger.log(LogStatus.INFO, "Test case Stated");
		browser.initialize_driver();
		rep.initializeReports();
		rep.Logger = rep.reports.startTest("Login Error Message");
		//browser.launchUrl();
		
		//tc1.tc1loginerrormessage();		
		
		String[][] data = datautil.readXlData("D:\\TechArch\\SalesForceFinalProject\\salesforcepro_excel\\TC2_LoginValidCredentials.xls", "Sheet1");
		String UserName = data[0][1];
		String password = data[1][1];
		WebElement Uname = browser.driver.findElement(By.xpath("//input[@id='username']"));
		fun.enterText(Uname, UserName, "UserName");
		WebElement pwd = browser.driver.findElement(By.id("password"));
		fun.enterText(pwd, password, "Password");
		pwd.clear();
		WebElement login = browser.driver.findElement(By.name("Login"));
		fun.clickBttn(login, "login");
		String errormessage = browser.driver.findElement(By.xpath("//div[@id='error']")).getText();
		System.out.println(errormessage+" error message");

		// getting Error Message
		WebElement errMessage = browser.driver.findElement(By.id("error"));

		// Validating Error Message
		String errMsg = "Please enter your password.";
		fun.errMessage(errMessage, errMsg);
		
		Thread.sleep(10000);
		rep.reports.endTest(rep.Logger);
		browser.driver.close();
		Thread.sleep(10000);

	}

	

	/*
	 * Name of the Method : TC3_Login_CheckRememberMe() Brief Description : Login
	 * and check Remember Me check box Arguments : None Creation Date :12/6/2019
	 * Last Modified :12/6/2019
	 */
	@Test(priority=2)
	public static void TC3_Login_CheckRememberMe() throws IOException, InterruptedException {
		System.out.println("TC3_Login_CheckRememberMe");
		
		browser.initialize_driver();
		rep.initializeReports();
		rep.Logger = rep.reports.startTest("Login with Remember Me checked");
		rep.Logger.log(LogStatus.INFO, "Test case Stated");

		tc3.TC3_Login_CheckRememberMe();
		
		Thread.sleep(10000);
		rep.reports.endTest(rep.Logger);

		browser.driver.close();
		Thread.sleep(10000);

	}

	
	  
	/*  Name of the Method : TC4_ForgotPassword()
	  Brief Description : Click Forgot
	  Password Arguments : None
	  Creation Date :12/7/2019 
	  Last Modified :12/7/2019
	  
	  */
	@Test(priority=3)
	  public static void TC4A_ForgotPassword() throws IOException,
	  InterruptedException
	  { 
		  System.out.println("TC4A_ForgotPassword");
			browser.initialize_driver();
			rep.initializeReports();
			  rep.Logger = rep.reports.startTest("Login with Remember Me checked");
				rep.Logger.log(LogStatus.INFO, "Test case Stated");

	  
	TC4A.TC4A_ForgotPassword();
	  
	  Thread.sleep(10000);
		rep.reports.endTest(rep.Logger);

		browser.driver.close();
		Thread.sleep(10000);

	  
	  }
	  
	  
	  
	
	  
	  /*
	  Name of the Method : TC5_UserMenu()
	   Brief Description : To verify User Menu drop down 
	  Arguments : None
	   Creation Date :12/7/2019
	    Last Modified :12/7/2019
	  */
	@Test(priority=4)
	  public static void TC5_UserMenu() throws IOException, InterruptedException {
	  System.out.println("TC5_UserMenu");
		browser.initialize_driver();
		rep.initializeReports();
		  rep.Logger = rep.reports.startTest("Login Valid Credentials");
			rep.Logger.log(LogStatus.INFO, "Test case Stated");

		tc5.tc5UserMenu();

		Thread.sleep(10000);
		rep.reports.endTest(rep.Logger);

		browser.driver.close();

	 }
	 	  
	
	/*  
	  
	  Name of the Method : TC4B_InvalidCredentials()
	  Brief Description : Enter invalid credentials 
	  Arguments : None 
	  Creation Date :12/7/2019
	  Last Modified  :12/7/2019	  
	  */
	@Test(priority=5)
	  public static void TC4B_LoginInvalidCredentials() throws IOException,
	  InterruptedException { 
		  System.out.println("TC4B_LoginInvalidCredentials");
			browser.initialize_driver();
			rep.initializeReports();
		  rep.Logger = rep.reports.startTest("Login Valid Credentials");
			rep.Logger.log(LogStatus.INFO, "Test case Stated");

			TC4B.tc4BLoginInvalidCredentials();
	  
	  Thread.sleep(10000);
	  rep.reports.endTest(rep.Logger);

	  browser.driver.close();
		Thread.sleep(10000);

	  
	  }
	  



}
